package com.bsa.homework.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.Query
import com.bsa.homework.entities.User

@Dao
interface UserDao {
    @Insert(onConflict = IGNORE)
    fun save(user: User)

    @Query("SELECT u.* FROM users u INNER JOIN posts p ON u.id = p.userId WHERE p.id = :postId")
    fun findUserByPostId(postId: Long): LiveData<User>
}