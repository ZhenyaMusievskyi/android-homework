package com.bsa.homework.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.bsa.homework.entities.Post

@Dao
interface PostDao {
    @Insert(onConflict = IGNORE)
    fun save(post: Post)

    @Insert(onConflict = REPLACE)
    fun saveAll(posts: List<Post>)

    @Query("SELECT * FROM posts")
    fun findAll(): LiveData<List<Post>>

    @Query("SELECT * FROM posts WHERE id = :id")
    fun findById(id: Long): LiveData<Post>

    @Query("SELECT count(*) > 0 FROM posts WHERE id = :id")
    fun hasPost(id: Long): Boolean
}