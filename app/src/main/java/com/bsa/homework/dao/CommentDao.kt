package com.bsa.homework.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.Query
import com.bsa.homework.entities.Comment

@Dao
interface CommentDao {
    @Insert(onConflict = IGNORE)
    fun saveAll(comments: List<Comment>)

    @Query("SELECT c.* FROM posts p INNER JOIN comments c ON c.postId = p.id WHERE p.id = :postId")
    fun findCommentsByPostId(postId: Long): LiveData<List<Comment>>
}