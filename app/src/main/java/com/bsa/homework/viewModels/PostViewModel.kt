package com.bsa.homework.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bsa.homework.databases.AppDatabase
import com.bsa.homework.entities.Post
import com.bsa.homework.repositories.PostRepository

class PostViewModel(application: Application): AndroidViewModel(application) {

    private val postRepository: PostRepository
    var allPosts: LiveData<List<Post>>

    init {
        val postDao = AppDatabase.getDatabase(application.applicationContext).PostDao()
        postRepository = PostRepository(postDao)
        allPosts = postRepository.getPosts()
    }
}