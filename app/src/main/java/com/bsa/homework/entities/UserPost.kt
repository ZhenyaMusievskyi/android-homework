package com.bsa.homework.entities

import androidx.room.Embedded
import androidx.room.Relation

data class UserPost(
    @Embedded val user: User,
    @Relation(
        parentColumn = "id",
        entityColumn = "userId"
    )
    val post: Post
)