package com.bsa.homework.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "comments")
public class Comment {
    @PrimaryKey
    private Long id;
    private String name;
    private String email;
    private String body;
    private Long postId;

    public Comment(Long id, String name, String email, String body, Long postId) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.body = body;
        this.postId = postId;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getBody() {
        return body;
    }

    public Long getPostId() {
        return postId;
    }
}
