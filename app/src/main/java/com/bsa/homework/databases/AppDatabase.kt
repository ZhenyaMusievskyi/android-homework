package com.bsa.homework.databases

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.bsa.homework.dao.CommentDao
import com.bsa.homework.dao.PostDao
import com.bsa.homework.dao.UserDao
import com.bsa.homework.entities.Comment
import com.bsa.homework.entities.Post
import com.bsa.homework.entities.User
import kotlinx.coroutines.CoroutineScope

@Database(entities = [Post::class, Comment::class, User::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun PostDao(): PostDao
    abstract fun CommentDao(): CommentDao
    abstract fun UserDao(): UserDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            val instance = Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "app_database"
            ).build()
            INSTANCE = instance
            return instance
        }
    }
}