package com.bsa.homework.repositories

import androidx.lifecycle.LiveData
import com.bsa.homework.dao.PostDao
import com.bsa.homework.dao.UserDao
import com.bsa.homework.entities.User
import com.bsa.homework.webServices.PostWebService
import com.bsa.homework.webServices.UserWebService
import kotlinx.coroutines.runBlocking
import retrofit2.Retrofit
import java.util.concurrent.Executor
import javax.inject.Inject
import javax.inject.Singleton

class UserRepository(
    private val userDao: UserDao,
    private val postDao: PostDao
) {
    private val userWebService: UserWebService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://jsonplaceholder.typicode.com")
            .build()
        userWebService = retrofit.create(UserWebService::class.java)
    }

    fun getUserByPostId(postId: Long): LiveData<User> {
        refreshUserByPostId(postId)
        return userDao.findUserByPostId(postId)
    }

    private fun refreshUserByPostId(postId: Long) {
        Thread {
            runBlocking {
                val user = userDao.findUserByPostId(postId).value
                if (user == null) {
                    val post = postDao.findById(postId).value
                    val response = userWebService.getUserById(post?.userId!!).execute()
                    userDao.save(response.body()!!)
                }
            }
        }.start()
    }
}