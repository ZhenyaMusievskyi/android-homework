package com.bsa.homework.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bsa.homework.dao.PostDao
import com.bsa.homework.entities.Post
import com.bsa.homework.webServices.PostWebService
import retrofit2.Retrofit
import kotlinx.coroutines.*
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.system.*


class PostRepository(private val postDao: PostDao) {
    private val postWebService: PostWebService

    private var posts: LiveData<List<Post>> = postDao.findAll()

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://jsonplaceholder.typicode.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        postWebService = retrofit.create(PostWebService::class.java)
    }

    fun getPosts(): LiveData<List<Post>> {
        if (posts.value == null) {
            refreshPosts()
            Thread.yield()
            posts = postDao.findAll()
        }
        return posts
    }

    fun getPostById(id: Long): LiveData<Post> {
        refreshPostById(id)
        return postDao.findById(id)
    }

    private fun refreshPosts() {
        Thread {
            runBlocking {
                val response = postWebService.getPosts()
                postDao.saveAll(response)
            }
        }.start()
    }

    private fun refreshPostById(id: Long) {
        val postExists = postDao.hasPost(id)
        if (!postExists) {
            val response = postWebService.getPostById(id).execute()
            postDao.save(response.body()!!)
        }
    }
}