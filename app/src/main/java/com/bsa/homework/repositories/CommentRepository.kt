package com.bsa.homework.repositories

import androidx.lifecycle.LiveData
import com.bsa.homework.dao.CommentDao
import com.bsa.homework.entities.Comment
import com.bsa.homework.webServices.CommentWebService
import retrofit2.Retrofit
import java.util.concurrent.Executor
import javax.inject.Inject
import javax.inject.Singleton

class CommentRepository (private val commentDao: CommentDao) {
    private val commentsWebService: CommentWebService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://jsonplaceholder.typicode.com")
            .build()

        commentsWebService = retrofit.create(CommentWebService::class.java)
    }

    fun getCommentsByPostId(postId: Long): LiveData<List<Comment>> {
        var comments = commentDao.findCommentsByPostId(postId)
        if (comments.value?.size == 0) {
            refreshCommentsByPostId(postId)
            comments = commentDao.findCommentsByPostId(postId)
        }
        return comments
    }

    private fun refreshCommentsByPostId(postId: Long) {
        val response = commentsWebService.getCommentsByPostId(postId).execute()
        val comments = response.body()
        commentDao.saveAll(comments!!)
    }
}