package com.bsa.homework.webServices

import com.bsa.homework.entities.Comment
import com.bsa.homework.entities.Post
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.Call

interface PostWebService {
    @GET("/posts")
    suspend fun getPosts() : List<Post>

    @GET("/posts/{id}")
    fun getPostById(@Path("id") id: Long): Call<Post>

    @GET("/posts/{postId}/comments")
    fun getPostComments(@Path("postId") postId: Long): Call<List<Comment>>
}