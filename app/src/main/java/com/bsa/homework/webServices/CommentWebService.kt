package com.bsa.homework.webServices

import com.bsa.homework.entities.Comment
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


interface CommentWebService {
    @GET("/comments?postId={id}")
    fun getCommentsByPostId(@Path("id") id: Long): Call<List<Comment>>
}