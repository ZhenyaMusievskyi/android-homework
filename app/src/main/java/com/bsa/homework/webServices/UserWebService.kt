package com.bsa.homework.webServices

import com.bsa.homework.entities.User
import retrofit2.Call
import retrofit2.http.GET

interface UserWebService {
    @GET("/user?id={id}")
    fun getUserById(id: Long): Call<User>
}